using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    public static GameplayManager Singleton;
    [SerializeField] SelectCharacterSO selectCharacterSO;

    private void Awake()
    {
        if (!ReferenceEquals(Singleton, null)) //Singleton != null
        {
            Debug.Log($"Only one instance of {nameof(GameplayManager)} can exist!");
            Destroy(gameObject);
        }

        Singleton = this;

        Instantiate(selectCharacterSO.CurrentSelectedSkin, transform.position, transform.rotation);
    }
}
