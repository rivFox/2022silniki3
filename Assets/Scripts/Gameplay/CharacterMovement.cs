﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterMovement : MonoBehaviour
{
    const float gravity = -9.81f;
    private CharacterController characterController;
    private float yMovement = gravity;

    [SerializeField] float velocity = 5f;
    [SerializeField] float jumpForce = 10f;
    [SerializeField] float jumpFall = 30f;

    private void Awake()
    {
        if (ReferenceEquals(GameplayManager.Singleton, null))
            enabled = false;

        characterController = GetComponent<CharacterController>();
    }

    private void Update()
    {
        var deltaTime = Time.deltaTime;
        var movementInput = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));

        //gracz porusza się w zalezności od rotacji, ale my tego nie chcemy
        //movementInput = transform.TransformDirection(movementInput);

        movementInput *= velocity;

        if(Input.GetKey(KeyCode.Space) && characterController.isGrounded)
        {
            yMovement = jumpForce;
        }
        yMovement = Mathf.Max(gravity, yMovement - (jumpFall * deltaTime));

        characterController.Move(new Vector3(movementInput.x, yMovement, movementInput.z) * deltaTime);
        if(Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0f || Mathf.Abs(Input.GetAxisRaw("Vertical")) > 0f)
            transform.rotation = Quaternion.LookRotation(movementInput, transform.up);
    }
}
