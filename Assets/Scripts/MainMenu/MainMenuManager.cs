using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] Button startGame;
    [SerializeField] Button settings;
    [SerializeField] Button exit;

    private void Awake()
    {
        startGame.onClick.AddListener(StartGame);
        settings.onClick.AddListener(() =>
        {
            //open settings
        });
        exit.onClick.AddListener(() => Application.Quit());
    }

    private void StartGame()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
    }
}
