using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectCharacterManager : MonoBehaviour
{
    [Header("SingletonSO")]
    [SerializeField] SelectCharacterSO selectCharacterSO;

    [Header("UI")]
    [SerializeField] Button leftArrowButton;
    [SerializeField] Button rightArrowButton;
    [SerializeField] TMP_Text characterName;
    [SerializeField] Button playButton;


    [Header("Scene Configurable")]
    [SerializeField] Transform skinSpawnTransform;

    private SkinController spawnedSkin;
    private SkinController spanwedSkinPrefab;

    private void Awake()
    {
        leftArrowButton.onClick.AddListener(() => SelectNextSkin(true));
        rightArrowButton.onClick.AddListener(() => SelectNextSkin());
        playButton.onClick.AddListener(() =>
        {
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
        });

        CleanupAndSpawnCurrentSkin();
    }

    private void CleanupAndSpawnCurrentSkin()
    {
        if(spawnedSkin)
            Destroy(spawnedSkin.gameObject);

        spawnedSkin = Instantiate(selectCharacterSO.CurrentSelectedSkin, skinSpawnTransform);
        spanwedSkinPrefab = selectCharacterSO.CurrentSelectedSkin;
        characterName.text = selectCharacterSO.CurrentSelectedSkin.name;
    }

    private void SelectNextSkin(bool back = false)
    {
        var skinId = selectCharacterSO.SkinPrefabs.IndexOf(spanwedSkinPrefab);
        if (back)
        {
            skinId--;
            if (skinId < 0)
                skinId = selectCharacterSO.SkinPrefabs.Count - 1;
        }
        else
        {
            skinId++;
            if (skinId > selectCharacterSO.SkinPrefabs.Count - 1)
                skinId = 0;
        }

        selectCharacterSO.CurrentSelectedSkin = selectCharacterSO.SkinPrefabs[skinId];
        CleanupAndSpawnCurrentSkin();
    }
}
