using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SelectCharacterSO", menuName = "SingletonSO/SelectCharacterSO")]
public class SelectCharacterSO : ScriptableObject
{
    public List<SkinController> SkinPrefabs;
    public SkinController CurrentSelectedSkin;
}
